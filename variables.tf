variable "tags" {
  type        = map(string)
  description = "(Optional) A mapping of tags which should be assigned to the resources. Any values provided here will be combined with a set of hardcoded tags in the module that cannot be overrided. Built-in values take precedence in the case of duplicate entries."
  default     = {}
}

variable "region" {
  type        = string
  description = "(Optional) The region to create the resources in."
  default     = "us-east-2"
}

variable "environment" {
  type        = string
  description = "(Required) The name of the environment these resources belong to. Valid values are dev, staging, or prod."

  validation {
    condition     = contains(["dev", "staging", "prod"], var.environment)
    error_message = "Value for var.environment must be one of either dev, staging, or prod."
  }
}

variable "aws_account_id" {
  type        = string
  description = "(Required) The Id of the account to allow to assume the role."
}
