terraform {
  required_version = "~> 0.14"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.34.0"
    }
    time = {
      source  = "hashicorp/time"
      version = ">= 0.7.0"
    }
  }
}

# Random string to use as suffix (allows for creating uniquely named resources, in cases such as for_each). Some may
# dislike this method, but I prefer to guarantee that the names are unique, and consume values from module output.
resource "random_string" "name_suffix" {
  length  = 8
  upper   = false
  special = false
}

# Using the time_static provider instead of the timestamp() function, because the latter will change on every plan.
resource "time_static" "current_time" {}

# Local values allow us to add hard-coded tags to user-provided tags, and specify other local variables used internally.
locals {
  tags = merge(var.tags, {
    environment = var.environment
    created_on  = time_static.current_time.rfc3339
  })
  ci_name_prefix = join("-", [var.environment, "ci"])
}

resource "aws_iam_group" "ci" {
  name = join("-", [local.ci_name_prefix, "group", random_string.name_suffix.result])
}

resource "aws_iam_user" "ci" {
  name = join("-", [local.ci_name_prefix, "user", random_string.name_suffix.result])

  tags = local.tags
}

resource "aws_iam_user_group_membership" "ci" {
  user = aws_iam_user.ci.name
  groups = [
    aws_iam_group.ci.name
  ]
}

resource "aws_iam_role" "ci" {
  name = join("-", [local.ci_name_prefix, "role", random_string.name_suffix.result])

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          "AWS" = "arn:aws:iam::${var.aws_account_id}:root"
        }
      },
    ]
  })

  tags = local.tags
}

resource "aws_iam_policy" "ci" {
  name        = join("-", [local.ci_name_prefix, "group", random_string.name_suffix.result])
  description = join(" ", ["Allows members to assume the role", aws_iam_role.ci.name])
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect   = "Allow",
        Action   = "sts:AssumeRole",
        Resource = aws_iam_role.ci.arn
    }]
  })
}

resource "aws_iam_group_policy_attachment" "ci" {
  group      = aws_iam_group.ci.name
  policy_arn = aws_iam_policy.ci.arn
}
