output "group_name" {
  value = aws_iam_group.ci.name
}

output "group_id" {
  value = aws_iam_group.ci.id
}

output "group_arn" {
  value = aws_iam_group.ci.arn
}

output "user_name" {
  value = aws_iam_user.ci.name
}

output "user_arn" {
  value = aws_iam_user.ci.arn
}

output "role_name" {
  value = aws_iam_role.ci.name
}

output "role_arn" {
  value = aws_iam_role.ci.arn
}
