# terraform-aws-iam-ci

This module creates the following resources in IAM:
- A role, with no permissions, which can be assumed by users within the same account,
- A policy, allowing users / entities to assume the above role,
- A group, with the above policy attached,
- A user, belonging to the above group.

These resources will be named according to the schema:

"{ var.environment }-ci-{ type }-{ random-string }"

## Usage in Terraform 0.14

```hcl
terraform {
  required_version = "~> 0.14"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.34.0"
    }
    time = {
      source  = "hashicorp/time"
      version = "0.7.0"
    }
  }
}

provider "aws" {
  region = "us-east-2"
}

provider "time" {}

module "iam" {
  source         = "/path/to/this/module" # or other source e.g registry, git
  region         = "us-east-2"
  environment    = "prod"
  aws_account_id = "111111111111"
}
```

## Providers

| Name | Version |
|------|---------|
| aws | >= 3.34.0 |
| random | n/a |
| time | >= 0.7.0 |

## Inputs

| Name | Description | Type | Default |
|------|-------------|------|---------|
| aws\_account\_id | (Required) The Id of the account to allow to assume the role. | `string` | n/a |
| environment | (Required) The name of the environment these resources belong to. Valid values are dev, staging, or prod. | `string` | n/a |
| region | (Optional) The region to create the resources in. | `string` | `"us-east-2"` |
| tags | (Optional) A mapping of tags which should be assigned to the resources. Any values provided here will be combined with a set of hardcoded tags in the module that cannot be overrided. Built-in values take precedence in the case of duplicate entries. | `map(string)` | `{}` |

## Outputs

| Name | Description |
|------|-------------|
| group\_arn | n/a |
| group\_id | n/a |
| group\_name | n/a |
| role\_arn | n/a |
| role\_name | n/a |
| user\_arn | n/a |
| user\_name | n/a |

