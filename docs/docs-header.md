# terraform-aws-iam-ci

This module creates the following resources in IAM:
- A role, with no permissions, which can be assumed by users within the same account,
- A policy, allowing users / entities to assume the above role,
- A group, with the above policy attached,
- A user, belonging to the above group.

These resources will be named according to the schema:

"{ var.environment }-ci-{ type }-{ random-string }"

## Usage in Terraform 0.14

```hcl
terraform {
  required_version = "~> 0.14"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.34.0"
    }
    time = {
      source  = "hashicorp/time"
      version = "0.7.0"
    }
  }
}

provider "aws" {
  region = "us-east-2"
}

provider "time" {}

module "iam" {
  source         = "/path/to/this/module" # or other source e.g registry, git
  region         = "us-east-2"
  environment    = "prod"
  aws_account_id = "111111111111"
}
```
