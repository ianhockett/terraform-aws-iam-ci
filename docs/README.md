To generate docs, run the following from the root of the repo:

```bash
$ terraform-docs . > README.md
```